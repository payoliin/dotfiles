local transparent = require("transparent")
transparent.clear_prefix("lualine")
transparent.clear_prefix("barbar")
transparent.clear_prefix("snacks")

local config = {

}

transparent.setup(config)

