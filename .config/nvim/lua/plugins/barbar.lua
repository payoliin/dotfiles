vim.keymap.set("n", "<Tab>", "<Cmd>BufferNext<Cr>")
vim.keymap.set("n", "<S-Tab>", "<Cmd>BufferPrevious<Cr>")
vim.keymap.set("n", '<A-">', "<Cmd>BufferGoto 1<Cr>")
vim.keymap.set("n", '<A-«>', "<Cmd>BufferGoto 2<Cr>")
vim.keymap.set("n", '<A-»>', "<Cmd>BufferGoto 3<Cr>")
vim.keymap.set("n", '<A-(>', "<Cmd>BufferGoto 4<Cr>")
vim.keymap.set("n", '<A-)>', "<Cmd>BufferGoto 5<Cr>")
vim.keymap.set("n", '<A-@>', "<Cmd>BufferGoto 6<Cr>")
vim.keymap.set("n", '<A-+>', "<Cmd>BufferGoto 7<Cr>")
vim.keymap.set("n", '<A-->', "<Cmd>BufferGoto 8<Cr>")
vim.keymap.set("n", '<A-/>', "<Cmd>BufferGoto 9<Cr>")
vim.keymap.set("n", '<A-p>', "<Cmd>BufferPin<Cr>")
vim.keymap.set("n", '<A-x>', "<Cmd>BufferClose!<Cr>")

require("barbar").setup{
  no_name_title = ""
}
