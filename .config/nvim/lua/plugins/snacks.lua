vim.keymap.set('n', '<Leader>e', ':lua Snacks.explorer()<Cr>')
vim.keymap.set('n', '<Leader>tm', ':lua Snacks.terminal()<Cr>')
vim.keymap.set('n', '<Leader>gg', ':lua Snacks.lazygit()<Cr>')
--for some reason this does not work as a function

local config = {
  explorer = {
    enabled = true,
    win = {
      list = {
        key = {
          ["<Leader>e"] = "explorer_focus",
        }
      }
    }
  },
  picker = {
    enabled = true,
  },
  lazygit = {
    enabled = true,
  },
  terminal = {
    enabled = true
  },
  indent = {
    enabled = true
  }
}

require("snacks").setup(config)
