vim.keymap.set('n',        ',', '<Plug>(leap)')
vim.keymap.set('n',        ';', '<Plug>(leap-backward)')
vim.keymap.set({'x', 'o'}, ',', '<Plug>(leap-forward)')
vim.keymap.set({'x', 'o'}, ';', '<Plug>(leap-backward)')
