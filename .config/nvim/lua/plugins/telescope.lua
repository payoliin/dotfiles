 require("telescope").setup{
  mappings = {
    i = {
      ["<C-t>"] = "move_selection_next",
      ["<C-s>"] = "move_selection_prev",
    }
  }
}
vim.keymap.set("n", "<Leader><Leader>", "<Cmd>Telescope find_files<Cr>")
vim.keymap.set("n", "<Leader>fa", "<Cmd>Telescope<Cr>")

