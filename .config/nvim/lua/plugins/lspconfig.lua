vim.keymap.set("n", "<Leader>gd", vim.lsp.buf.definition)
vim.keymap.set("n", "<Leader>gD", vim.lsp.buf.declaration)
vim.keymap.set("n", "<Leader>H", vim.lsp.buf.hover)
vim.keymap.set("n", "<Leader>ca", vim.lsp.buf.code_action)
vim.keymap.set(function ()
    vim.lsp.buf.code_action{ range = true }
end, "<Leader>ca", "v")
vim.keymap.set("n", "<Leader>gi", vim.lsp.buf.implementation)
vim.keymap.set("n", "<Leader>gr", vim.lsp.buf.references)
vim.keymap.set("n", "<Leader>ge", vim.diagnostic.goto_next)
vim.keymap.set("n", "<Leader>gE", vim.diagnostic.goto_prev)

vim.keymap.set("n", "<Leader>tt", vim.lsp.buf.format)
