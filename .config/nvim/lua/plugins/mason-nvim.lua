local on_attach = function (_, bufnr)  
    local opts = { silent = true, buffer = bufnr }

    vim.keymap.set("n", "<Leader>gd", vim.lsp.buf.definition, opts)
    vim.keymap.set("n", "<Leader>gD", vim.lsp.buf.declaration, opts)
    vim.keymap.set("n", "<Leader>H", vim.lsp.buf.hover, opts)
    vim.keymap.set("n", "<Leader>ca", vim.lsp.buf.code_action, opts)
    vim.keymap.set("v", "<Leader>ca", vim.lsp.buf.range_code_action, opts)
    vim.keymap.set("n", "<Leader>gi", vim.lsp.buf.implementation, opts)
    vim.keymap.set("n", "<Leader>gr", vim.lsp.buf.references, opts)
    vim.keymap.set("n", "<Leader>ge", vim.diagnostic.goto_next, opts)
    vim.keymap.set("n", "<Leader>gE", vim.diagnostic.goto_prev, opts)
end
require("mason").setup()
require("mason-lspconfig").setup{
    on_attach = on_attach
}

local lspcf = require("lspconfig")
local lsps = {"lua_ls", "clangd", "zls", "html", "cssls", "asm_lsp"}

for _, lsp in ipairs(lsps) do
    lspcf[lsp].setup {
        capabilities = require("cmp_nvim_lsp").default_capabilities()

    }
end

lspcf.lua_ls.setup {}
lspcf.clangd.setup {}

