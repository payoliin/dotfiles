vim.opt.completeopt = {'menu', 'menuone', 'noselect'}

-- Clipboard
vim.opt.clipboard = 'unnamedplus'

-- Tab
vim.opt.tabstop = 2                 -- number of visual spaces per TAB
vim.opt.softtabstop = 2             -- number of spacesin tab when editing
vim.opt.shiftwidth = 2              -- insert 4 spaces on a tab
vim.opt.expandtab = true            -- tabs are spaces, mainly because of python

-- UI config
vim.opt.number = true               -- show absolute number
vim.cmd[[autocmd InsertEnter * set relativenumber]]
vim.cmd[[autocmd InsertLeave * set norelativenumber]]
vim.opt.cursorline = true           -- highlight cursor line underneath the cursor horizontally
vim.opt.splitbelow = true           -- open new vertical split bottom
vim.opt.splitright = true           -- open new horizontal splits right
vim.opt.termguicolors = true        -- enabl 24-bit RGB color in the TUI
vim.opt.showmode = false            -- we are experienced, wo don't need the "-- INSERT --" mode hint

-- Searching
vim.opt.incsearch = true            -- search as characters are entered
vim.opt.hlsearch = false            -- do not highlight matches
vim.opt.ignorecase = true           -- ignore case in searches by default
vim.opt.smartcase = true            -- but make it case sensitive if an uppercase is entered

-- Allow quitting empty buffers
vim.api.nvim_create_autocmd("InsertLeave", {
  callback = function()
    if vim.bo.buftype == '' and vim.bo.modified then
      vim.bo.modified = false
    end
  end
})
