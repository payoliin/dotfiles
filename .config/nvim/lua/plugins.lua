require "paq" {
    "savq/paq-nvim",

    -- eye candy 
    "ray-x/starry.nvim",
    "nvim-lualine/lualine.nvim",
   "stevearc/dressing.nvim",
    "nvim-tree/nvim-web-devicons",

    -- snacks
    "folke/snacks.nvim",

    -- file stuff
    "nvim-telescope/telescope.nvim",
    "romgrk/barbar.nvim",
    "nvim-lua/plenary.nvim",
    "xiyaowong/transparent.nvim",

    -- text and movement
    "kylechui/nvim-surround",
    "numToStr/Comment.nvim",
    "ggandor/leap.nvim",
    "windwp/nvim-autopairs",

    -- lspconfig
    "neovim/nvim-lspconfig",
    "williamboman/mason.nvim",
    "williamboman/mason-lspconfig.nvim",

    -- cmp
    "hrsh7th/nvim-cmp",
    "hrsh7th/cmp-nvim-lsp",
    "hrsh7th/cmp-buffer",
    "hrsh7th/cmp-path",
    "hrsh7th/cmp-cmdline",
}

local pfile = io.popen("ls $HOME/.config/nvim/lua/plugins")
if pfile == nil then return end
local i=0
for filename in pfile:lines() do
    i = i+1
    local formatted = "plugins." .. filename:gsub("%.lua$", "")
    local success, err = pcall(require, formatted)
    if not success then
	    print("File " .. formatted .. " is not a valid lua file: " .. err)
    end
end

pfile:close()
