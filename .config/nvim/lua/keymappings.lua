vim.g.mapleader = " "
vim.keymap.set('n', '<C-c>', '<C-w>h', opts)
vim.keymap.set('n', '<C-t>', '<C-w>j', opts)
vim.keymap.set('n', '<C-s>', '<C-w>k', opts)
vim.keymap.set('n', '<C-r>', '<C-w>l', opts)
vim.keymap.set('n', '<C-l>', '<Cmd>redo<Cr>')
vim.keymap.set('n', '<C-,>', '$a;<Esc>o')

vim.keymap.set('i', '<C-e>', '<Esc>$a')
vim.keymap.set('i', '<C-i>', '<Esc>^i')
vim.keymap.set('i', '<C-u>', '<Esc>$a;<Cr>')
